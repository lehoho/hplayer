#ifndef HFFDECODETHREAD_H_
#define HFFDECODETHREAD_H_

struct AVPacket;
class HFFDecode;
#include <list>
#include <mutex>

#include "hthread.h"
#include "HFFDecode.h"

struct AVCodecParameters;

class HFFDecodeThread: public HFFDecode,  public HThread
{
public:
	HFFDecodeThread();
	virtual ~HFFDecodeThread();
	
	//清理队列
	virtual void Clear();

	//清理资源，停止线程
	virtual void Close();

	virtual int stop();

	//取出一帧数据，并出栈，如果没有返回NULL
	virtual AVPacket *Pop();

	virtual void Push(AVPacket *pkt);

	//最大队列
	int maxList = 10;
	bool isExit = false;

protected:
	std::list <AVPacket *> packs;
};


#endif
