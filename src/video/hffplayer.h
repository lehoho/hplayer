#ifndef H_FFPLAYER_H
#define H_FFPLAYER_H

#include "HVideoPlayer.h"
#include "ffmpeg_util.h"

#include "hthread.h"

#include <atomic>

struct HFFVideoPlayer;
struct HFFAudioPlayer;

class HFFPlayer : public HVideoPlayer, public HThread {
public:
    HFFPlayer();
    ~HFFPlayer();

    virtual int stop();

    virtual int start();

    virtual int pause();
    
    virtual int resume();

    virtual int seek(int64_t ms);

private:
    virtual bool doPrepare();
    virtual void doTask();//在此函数中进行读帧、解码、图像处理等操作
    virtual bool doFinish();

    int open();
    int close();

public:
    int64_t block_starttime;
    int64_t block_timeout;
    int     quit;
    HFFVideoPlayer* videoPlayer;
    HFFAudioPlayer* audioPlayer;

private:
    static std::atomic_flag s_ffmpeg_init;

    AVDictionary*       fmt_opts;
    AVDictionary*       codec_opts;
    AVFormatContext*    fmt_ctx;

    int video_stream_index;
    int audio_stream_index;
    int subtitle_stream_index;

    int video_time_base_num;
    int video_time_base_den;

};

#endif // H_FFPLAYER_H
