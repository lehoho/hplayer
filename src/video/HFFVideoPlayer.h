#ifndef HFFVIDEOPLAYER_H_
#define HFFVIDEOPLAYER_H_

#include <fstream>

#include "HFFDecodeThread.h"
#include "hframe.h"

extern "C"
{
#include "libavutil/avutil.h"
}

struct SwsContext;

typedef void (*OnDecodeCallback)(void* hp, void* frame);

class HFFVideoPlayer : public HFFDecodeThread
{

public:
    HFFVideoPlayer();
    virtual ~HFFVideoPlayer();

    virtual void doTask(); 

    void SetHP(void* hp) { m_hp = hp ;}

    void SetOnDecodeCallback(OnDecodeCallback cb) { m_onDecodeCallback = cb; }

    //打开解码器
    virtual bool Open();

    //
    virtual void Close();

    //同步时间，由外部传入
	long long synpts = 0;


private:
    virtual bool doPrepare();
    virtual bool doFinish();

    void* m_hp;
    OnDecodeCallback m_onDecodeCallback;

    // for scale
    AVPixelFormat   src_pix_fmt;
    AVPixelFormat   dst_pix_fmt;
    SwsContext*     sws_ctx;
    uint8_t*        data[4];
    int             linesize[4];
    HFrame          hframe;

    bool OpenSwsCtx();
};

#endif
