#include "HFFDecodeThread.h"
#include "HFFDecode.h"
#include <thread>
#include <iostream>

#include "hlog.h"
//清理资源，停止线程
void HFFDecodeThread::Close()
{
	hlogi("HFFDecodeThread::Close()");
	Clear();
	HFFDecode::Close();
	//等待线程退出
	isExit = true;
}


int HFFDecodeThread::stop()
{
	isExit = true;
	return HThread::stop();
}

void HFFDecodeThread::Clear()
{
	m_mux.lock();
	while (!packs.empty())
	{
		AVPacket *pkt = packs.front();
		XFreePacket(&pkt);
		packs.pop_front();
	}

	m_mux.unlock();
}


//取出一帧数据，并出栈，如果没有返回NULL
AVPacket *HFFDecodeThread::Pop()
{
	m_mux.lock();
	if (packs.empty())
	{
		m_mux.unlock();
		return NULL;
	}
	AVPacket *pkt = packs.front();
	packs.pop_front();
	m_mux.unlock();
	return pkt;
}


void HFFDecodeThread::Push(AVPacket *pkt)
{
	if (!pkt)return;
	//阻塞
	while (!isExit)
	{
		m_mux.lock();
		if (packs.size() < maxList)
		{
			packs.push_back(pkt);
			m_mux.unlock();
			break;
		}
		m_mux.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}


HFFDecodeThread::HFFDecodeThread()
{
	hlogi("HFFDecodeThread::HFFDecodeThread()");
}


HFFDecodeThread::~HFFDecodeThread()
{	
	hlogi("HFFDecodeThread::~HFFDecodeThread()");
	isExit = true;
}
