#ifndef HFFDECODE_H_
#define HFFDECODE_H_
struct AVCodecParameters;
struct AVCodecContext;
struct AVFrame;
struct AVPacket;

#include <mutex>
extern "C"
{
#include<libavutil/rational.h>
}


extern void XFreePacket(AVPacket **pkt);
extern void XFreeFrame(AVFrame **frame);

enum {
    SOFTWARE_DECODE         = 1,
    HARDWARE_DECODE_QSV     = 2,
    HARDWARE_DECODE_CUVID   = 3,
};
#define DEFAULT_DECODE_MODE HARDWARE_DECODE_CUVID


class HFFDecode
{
public:
    bool isAudio = false;

    //当前解码到的pts
    long long pts = 0;

    int  decode_mode;
    int  real_decode_mode;

    void SetPara(AVCodecParameters *pa) { para = pa; }
    
    //打开解码器
    virtual bool Open();

    //发送到解码线程，不管成功与否都释放pkt空间（对象和媒体内容）
    virtual bool Send(AVPacket *pkt);

    //获取解码数据，一次send可能需要多次Recv，获取缓冲中的数据Send NULL在Recv多次
    //每次复制一份，由调用者释放 av_frame_free
    virtual AVFrame* Recv();

    virtual void Close();
    virtual void Clear();

    void set_decode_mode(int mode) { decode_mode = mode;}

    void set_time_base(AVRational time_base) {m_time_base = time_base; }

    HFFDecode();
    virtual ~HFFDecode();

protected:
    AVCodecContext *codec_ctx = 0;
    std::mutex m_mux;
    AVCodecParameters *para;
    AVRational m_time_base;
    
private:
    

};


#endif