#include "CentralWidget.h"
#include "qtstyles.h"

CentralWidget::CentralWidget(QWidget *parent) : QWidget(parent)
{
    initUI();
    initConnect();
}

CentralWidget::~CentralWidget() {
    hlogd("~CentralWidget");
}

void CentralWidget::initUI() {
    lside = new LsideWidget;//左边栏
    mv = new HMultiView;
    rside = new RsideWidget;//右边栏

    QSplitter *split = new QSplitter(Qt::Horizontal);//分裂器
    split->addWidget(lside);
    split->addWidget(mv);
    split->addWidget(rside);

    lside->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);//分别设置水平方向和竖直方向的策略
    lside->setMinimumWidth(LSIDE_MIN_WIDTH);
    lside->setMaximumWidth(LSIDE_MAX_WIDTH);

    mv->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mv->setMinimumWidth(MV_MIN_WIDTH);
    mv->setMinimumHeight(MV_MIN_HEIGHT);

    rside->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    rside->setMinimumWidth(RSIDE_MIN_WIDTH);
    rside->setMaximumWidth(RSIDE_MAX_WIDTH);

    split->setStretchFactor(0, 1);
    split->setStretchFactor(1, 1);
    split->setStretchFactor(2, 1);

    QHBoxLayout *hbox = genHBoxLayout();
    hbox->addWidget(split);
    setLayout(hbox);
}

void CentralWidget::initConnect() {

}
