#ifndef HFFAUDIOPLAYER_H
#define HFFAUDIOPLAYER_H

#include "HFFDecodeThread.h"
#include <fstream>

class XAudioPlay;
class XResample;

class HFFAudioPlayer : public HFFDecodeThread
{
public:
    HFFAudioPlayer(int sampleRate,int channels);
    HFFAudioPlayer();
    virtual ~HFFAudioPlayer();

    virtual void doTask(); 

    virtual bool doPrepare();
    
    virtual bool doFinish();

    virtual int pause();

    virtual int resume(); 

    //打开解码器
    virtual bool Open();

    //
    virtual void Close();

private:
    unsigned char *m_pcm_buf;
    XResample* m_resample;
    XAudioPlay* m_audioPlay;
};



#endif
